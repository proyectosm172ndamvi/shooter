using System;
using System.Collections;
using System.Collections.Generic;
using Unity.AI.Navigation;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Rendering.PostProcessing;
using MinAttribute = UnityEngine.MinAttribute;

public class EnemyController : MonoBehaviour, IDamageable
{
    public event Action<int> OnDamage;

    private enum SwitchMachineStates { NONE, IDLE, WALK, SHOOT, PATRULLA, MUERTO }; //enum de los estados de la maquina de estados
    [SerializeField]
    private SwitchMachineStates m_CurrentState; //estado actual de la maquina    
    private NavMeshAgent m_NavMeshAgent;
    private Vector3 m_destination;
    private Vector3 m_ShootDirection;
    [SerializeField]
    private bool shooting;

    [Header("Stats")]
    [SerializeField]
    private float m_Speed = 3f;
    [SerializeField]
    [Min(0)]
    private int porcentajeError;
    [SerializeField]
    [Min(1)]
    private float m_RadioDeteccion;
    [SerializeField]
    [Min(1)]
    private float m_RadioDisparo;
    [SerializeField]
    private float m_tiempoAntesPatrulla;
    [SerializeField]
    private int vida;

    [Header("LayerMasks")]
    [SerializeField]
    private LayerMask m_PlayerMask; //la del player
    [SerializeField]
    private LayerMask m_VisionMask; //la de los objetos que le tapan la vista

    [Header("BodyParts")]
    [SerializeField]
    private Rigidbody[] bodyParts;

    [Header("Animator")]
    [SerializeField]
    private Animator animator;
    //[SerializeField]
    //private Animation[] listaAnimaciones;

    public void Damage(int amount)
    {
        print("HE PERDIDO " + amount + " DE VIDA");
        vida -= amount;
        if (vida <= 0 && m_CurrentState != SwitchMachineStates.MUERTO) {
            ChangeState(SwitchMachineStates.MUERTO);
        }
    }

  

    private void Awake()
    {       
        m_NavMeshAgent = GetComponent<NavMeshAgent>();
        foreach (Rigidbody part in bodyParts)
        {
            part.gameObject.GetComponent<HurtBox>().OnDamage += Damage;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }

    // Update is called once per frame
    void Update()
    {
        Detectar();
        UpdateState();
    }

   

    //----------------------------------[ M�QUINA DE ESTADOS }----------------------------------//
    private void ChangeState(SwitchMachineStates newState)
    {
        //if (newState == m_CurrentState)
        //    return;

        ExitState();
        InitState(newState);
    }


    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                animator.SetBool("Moving", false);
                m_NavMeshAgent.speed = 0;
                StartCoroutine(meVoyPatrullar());
                break;
            case SwitchMachineStates.WALK:
                animator.SetBool("Moving", true);
                m_NavMeshAgent.speed = m_Speed;                
                break;
            case SwitchMachineStates.SHOOT:
                animator.SetBool("Moving", false);
                m_NavMeshAgent.speed = 0;
                StartCoroutine(Shoot());
                break;
            case SwitchMachineStates.PATRULLA:
                animator.SetBool("Moving", true);
                m_NavMeshAgent.speed = m_Speed;
                StartCoroutine(BuscoPuntoDePatrulla());
                break;
            case SwitchMachineStates.MUERTO:
                StopAllCoroutines();
                foreach (Rigidbody part in bodyParts)
                {
                    part.gameObject.GetComponent<HurtBox>().OnDamage -= Damage;
                    part.isKinematic = false;
                }
                animator.enabled = false;
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
               
                break;
            case SwitchMachineStates.WALK:
                StopAllCoroutines();
                m_NavMeshAgent.SetDestination(m_destination);
                ARango();
                break;
            case SwitchMachineStates.SHOOT:
                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.SHOOT:
                StopAllCoroutines();
                break;
            case SwitchMachineStates.PATRULLA:
                StopAllCoroutines();
                break;
            default:
                break;
        }
    }

    private void Detectar() {
        //RaycastHit[] objetivos;
        Collider[] objetivo = Physics.OverlapSphere(this.transform.position, m_RadioDeteccion, m_PlayerMask);
        if (objetivo.Length > 0)
        {
            RaycastHit hit;
            if (Physics.Raycast(this.transform.position, (objetivo[0].transform.position - this.transform.position).normalized, out hit, m_RadioDeteccion, m_VisionMask))
            {
                if (hit.collider.gameObject.CompareTag("Player") && !shooting)
                {
                    m_destination = hit.point;
                    ChangeState(SwitchMachineStates.WALK);
                }
            }
        }
        else if(m_NavMeshAgent.remainingDistance <= 1  && m_CurrentState != SwitchMachineStates.IDLE) { 
            ChangeState(SwitchMachineStates.IDLE);
        }
        
    }

    private void ARango() {
        Collider[] objetivo = Physics.OverlapSphere(this.transform.position, m_RadioDisparo, m_PlayerMask);
        
        if (objetivo.Length > 0)
        {
            m_ShootDirection = (objetivo[0].transform.position - this.transform.position).normalized;
            shooting = true;
            ChangeState(SwitchMachineStates.SHOOT);
            
        }
        else shooting = false;
    }

    private IEnumerator Shoot() {
        RaycastHit hit;
        if (porcentajeError > UnityEngine.Random.Range(1, 101)) { 
            m_ShootDirection -= new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-0.4f, 3f), 0);
        }
        if (Physics.Raycast(this.transform.position,m_ShootDirection, out hit, m_RadioDisparo, m_VisionMask))
        {
            Debug.DrawLine(this.transform.position, hit.point, Color.yellow, 2f);
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                    target.Damage(5);
            }
        }
        yield return new WaitForSeconds(0.5f);
        ARango();
    }

    private IEnumerator meVoyPatrullar() {
        yield return new WaitForSeconds(m_tiempoAntesPatrulla);
        ChangeState(SwitchMachineStates.PATRULLA);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(this.transform.position, m_RadioDeteccion);
    }

    private IEnumerator BuscoPuntoDePatrulla() {
        while (true) {            
            if (m_NavMeshAgent.remainingDistance <= 1.2f)
            {
                animator.SetBool("Moving", true);
                NavMeshHit hit;
                if (NavMesh.SamplePosition(new Vector3(UnityEngine.Random.Range(-28, 27), 0, UnityEngine.Random.Range(-21, 25.5f)), out hit, 0.3f, NavMesh.AllAreas))
                {
                    m_destination = hit.position;
                    m_NavMeshAgent.SetDestination(m_destination);
                }
            }
            yield return new WaitForSeconds(5);
        }
        
    }
}
