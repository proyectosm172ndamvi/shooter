using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtBox : MonoBehaviour, IDamageable, IPushable
{
    public event Action<int> OnDamage;
    [SerializeField]
    private int MultiplicadorDaņo;

    public void Damage(int amount)
    {
        OnDamage?.Invoke(amount * MultiplicadorDaņo);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Push(Vector3 direction, float impulse)
    {
        this.gameObject.GetComponent<Rigidbody>().AddForce(direction *impulse, ForceMode.Impulse);
    }
}
