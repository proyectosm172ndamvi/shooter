using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class SpyCamController : MonoBehaviour
{
    private bool resto=true;
    [SerializeField]
    private float degreeRotationXFrame;
    [SerializeField]
    private ScriptableRendererFeature RendererFeature = null;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Inspeccionar());
        RendererFeature.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator Inspeccionar() { 
        while (true)
        {
            if (resto)
            {
                this.transform.localEulerAngles = new Vector3(0, this.transform.localEulerAngles.y - degreeRotationXFrame, 0);
                if(this.transform.localEulerAngles.y <=160)
                    resto=false;
            }
            else
            {
                this.transform.localEulerAngles = new Vector3(0, this.transform.localEulerAngles.y + degreeRotationXFrame, 0);
                if (this.transform.localEulerAngles.y >= 200)
                    resto = true;
            }
            yield return null;
        }
    }

    public void changeRenderFeatureState() {
        RendererFeature.SetActive(!RendererFeature.isActive);
    }
}
