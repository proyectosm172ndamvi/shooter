using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class CAmeraConroller : MonoBehaviour
{
    [SerializeField]
    bool activated;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void toggleCamera() {
        if (activated)
        {
            this.GetComponent<Camera>().depth=-1;
            activated = false;
        }
        else {
            this.GetComponent<Camera>().depth = 0;
            activated= true;
        }
    }
}
