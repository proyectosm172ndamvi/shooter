using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PIstolaVisualizer : MonoBehaviour
{
    [SerializeField]
    GameObject[] listaArmas;
    [SerializeField]
    Animator animator;
    private GameObject m_currentArma;
    // Start is called before the first frame update
    void Start()
    {
        m_currentArma = listaArmas[0];
        animator.Play("Sacala");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CambiarArma(int i) { 
        m_currentArma.SetActive(false);
        listaArmas[i].SetActive(true);
        m_currentArma = listaArmas[i];
        animator.Play("Sacala");
    }
}
