using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

public class PlayerController : MonoBehaviour, IDamageable
{
    private enum SwitchMachineStates { NONE, IDLE, WALK, SHOOT }; //enum de los estados de la maquina de estados
    [SerializeField]
    private SwitchMachineStates m_CurrentState; //estado actual de la maquina
    private Rigidbody m_Rigidbody;
    [SerializeField]
    private Camera m_Camera;
    [SerializeField]
    private GameObject spyCamera;


    [Header("Inputs")]
    [SerializeField]
    private InputActionAsset m_InputActionAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;
    private InputAction m_MouseMovementAction;

    [Header("Stats")]
    [SerializeField]
    private float m_RotationSpeed = 180f;
    [SerializeField]
    private float m_Speed = 3f;
    [SerializeField]
    private float maxAngleRotationY;
    [SerializeField]
    private float minAngleRotationY;
    [SerializeField]
    private bool shootgunShoot;
    [SerializeField]
    private bool onCooldown;
    [SerializeField]
    private int vida;

    [Header("LayerMasks")]
    [SerializeField]
    private LayerMask m_ShootMask;

    [Header("Events")]
    [SerializeField]
    private GameEvent toggleCamera;
    [SerializeField]
    private GEGenericoInt CambioArma;

    Vector3 m_Movement = Vector3.zero;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Input = Instantiate(m_InputActionAsset); //Instanciem el asset (pf)
        m_Input.FindActionMap("Default").FindAction("Shoot").performed += HacerPium;
        m_Input.FindActionMap("Default").FindAction("ChangeCamera").performed += ChangeCamera;
        m_Input.FindActionMap("Default").FindAction("ShootSpyCam").performed += ColocarSpyCam;
        m_MovementAction = m_Input.FindActionMap("Default").FindAction("Movement");
        m_MouseMovementAction = m_Input.FindActionMap("Default").FindAction("MousePosition");
        m_Input.FindActionMap("Default").Enable();
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void OnDestroy()
    {
        
    }
    private void OnDisable()
    {
        StopAllCoroutines();
        m_Input.FindActionMap("Default").FindAction("Shoot").performed -= HacerPium;
        m_Input.FindActionMap("Default").FindAction("ChangeCamera").performed -= ChangeCamera;
        m_Input.FindActionMap("Default").FindAction("ShootSpyCam").performed -= ColocarSpyCam;
        m_Input.FindActionMap("Default").Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        onCooldown = false;
        InitState(SwitchMachineStates.IDLE);
    }

    // Update is called once per frame
    void Update()
    {
        MouseRotation();
        UpdateState();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "AreaPistola")
        {
            CambioArma.Raise(0);
            shootgunShoot = false;
        }
        else if (other.tag == "AreaShotgun") {
            CambioArma.Raise(1);
            shootgunShoot=true;
        }
    }

    //----------------------------------[ M�QUINA DE ESTADOS }----------------------------------//
    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }


    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Rigidbody.velocity = Vector3.zero;
                break;
            case SwitchMachineStates.WALK:
                break;
            case SwitchMachineStates.SHOOT:
                Shoot();
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                //MouseRotation();
                if (m_MovementAction.ReadValue<Vector3>().x != 0 || m_MovementAction.ReadValue<Vector3>().y != 0 || m_MovementAction.ReadValue<Vector3>().z != 0)
                    ChangeState(SwitchMachineStates.WALK);
                break;
            case SwitchMachineStates.WALK:
                //MouseRotation();
                m_Movement = Vector3.zero;
                if (m_MovementAction.ReadValue<Vector3>().z > 0)
                {
                    m_Movement += transform.forward;
                }
                else if (m_MovementAction.ReadValue<Vector3>().z < 0)
                {
                    m_Movement -= transform.forward;
                }
                if (m_MovementAction.ReadValue<Vector3>().x > 0)
                {
                    m_Movement += transform.right;
                }
                else if (m_MovementAction.ReadValue<Vector3>().x < 0)
                {
                    m_Movement -= transform.right;
                }
                
                m_Rigidbody.velocity = m_Movement.normalized * m_Speed + Vector3.up * m_Rigidbody.velocity.y;
                if (m_Rigidbody.velocity == Vector3.zero) //mirar esto por si caes de un sitio alto 
                    ChangeState(SwitchMachineStates.IDLE);
                break;
            case SwitchMachineStates.SHOOT:
                ChangeState(SwitchMachineStates.IDLE);
                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            default:
                break;
        }
    }

    float yRotate = 0;

    public event Action<int> OnDamage;

    private void MouseRotation() {
        transform.Rotate(0, m_MouseMovementAction.ReadValue<Vector2>().x * m_RotationSpeed * Time.deltaTime,0);
        
        // = m_Camera.transform.localEulerAngles.x;
        yRotate -= m_MouseMovementAction.ReadValue<Vector2>().y * m_RotationSpeed * Time.deltaTime;
        yRotate = Mathf.Clamp(yRotate, minAngleRotationY, maxAngleRotationY);
        m_Camera.transform.localEulerAngles = Vector3.right * yRotate;
    }

    private void Shoot()
    {
        RaycastHit hit;
        if (!shootgunShoot)
        {
            if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, 20f, m_ShootMask))
            {
                Debug.Log($"He tocat {hit.collider.gameObject} a la posici {hit.point} amb normal {hit.normal}");
                Debug.DrawLine(m_Camera.transform.position, hit.point, Color.green, 2f);

                if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                    target.Damage(10);

                if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
                    pushable.Push(m_Camera.transform.forward, 100);
            }
        }
        else { 
            for (int i = 0; i < 6; i++)
            {                
                Vector3 directionRand = m_Camera.transform.forward - new Vector3(Random.Range(-0.2f,0.2f), Random.Range(-0.2f, 0.2f), 0);
                if (Physics.Raycast(m_Camera.transform.position, directionRand, out hit, 20f, m_ShootMask))
                {
                    //print(directionRand+" -->"+i);
                    Debug.Log($"He tocat {hit.collider.gameObject} a la posici {hit.point} amb normal {hit.normal}");
                    Debug.DrawLine(m_Camera.transform.position, hit.point, Color.green, 2f);

                    if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                        target.Damage(12);

                    if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
                        pushable.Push(m_Camera.transform.forward, 300);
                }
            }
            StartCoroutine(ShootCooldown());
        }
    }

    private void HacerPium(InputAction.CallbackContext actionContext) {
        if(!onCooldown)
            ChangeState(SwitchMachineStates.SHOOT);
    }

    public IEnumerator ShootCooldown() {
        onCooldown = true;
        yield return new WaitForSeconds(1);
        onCooldown=false;
    }

    private void ChangeCamera(InputAction.CallbackContext actionContext) { 
        toggleCamera.Raise();
    }

    private void ColocarSpyCam(InputAction.CallbackContext actionContext) {
        RaycastHit hit;
        if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, 40f, m_ShootMask))
        {
            
            Debug.DrawLine(m_Camera.transform.position, hit.point, Color.magenta, 2f);

            spyCamera.transform.position = hit.point;
            spyCamera.transform.forward = -hit.normal;
        }
    }

    public void Damage(int amount)
    {
        vida-= amount;
        if (vida <= 0) { 
            this.gameObject.SetActive(false);
        }
    }
}
